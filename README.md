# Chillisoft

## Getting started

Clone the repo fist
```
git clone git@gitlab.com:igvanniekerk/chillisoft-v2.git
```
Node version v14.20.1 used for Develop

Start by running, installing Typescript Globally
```
npm install -g typescript
```
```
npm install
```
TSC is to compile and build js file
```
tsc
```

## Running Application 

```

node app.js users.txt menus.txt > output.json

```