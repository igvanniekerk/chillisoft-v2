import * as fs from 'fs';


try {
  if (process.argv.length !== 4) {
    console.error('Usage: node app.js <file1> <file2>');
    process.exit(1);
  }

  let menuPath: string = '';
  let userPath: string= '';
  process.argv.forEach((item) => {
    if (item?.toLocaleLowerCase()?.includes('users.txt')) {
      userPath = item
    } else if (item?.toLocaleLowerCase()?.includes('menus.txt')) { 
      menuPath = item
    }
  })

  if (!menuPath && !userPath) {
    throw new Error(`Upload both files`);
  }

  const contentMenu: string[] = readFileContent(menuPath);
  const contentUser: string[] = readFileContent(userPath);

  if (!contentUser?.length && !contentMenu?.length) {
    throw new Error(`Files not converted`);
  }


  const users = {"users": userFileBase(contentUser, contentMenu) }


  // Check if the output is being redirected
  if (process.stdout.isTTY) {
    let outputFilePath: string = 'output.json';
    fs.writeFileSync(outputFilePath, JSON.stringify(users, null, 2));
  } else {
    console.log(JSON.stringify(users, null, 2))
  }

  } catch (error: any) {
    console.error(`Error : ${error.message}`);
    process.exit(1);
  }




////////////////////////////////FUNCTIONS\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

/**
 * Random number generator
 * 
 * @param menusLength 
 * @returns number[]
 */

function randomNumberGen(menusLength: number ): number[]{
  return Array(10).fill(0).map(() => Math.round(Math.random() * menusLength))
}


/**
 * validates and checks the menu 
 * 
 * @param menus 
 * @returns string[]
 */
function menuCheck(menus: any[]): string[]{
  try {
    const menuRegex = /^\d+,\s*.*?\bmenu\b/i;
    const isPassed = menus.reduce(
    (accumulator, currentValue) => {
      return accumulator && menuRegex.test(currentValue)},
    true,
  );
  
  if(isPassed){
    return menus.map((menu)=> menu.substring(menu.indexOf(',') + 1)?.trim())
  }
  throw new Error(`Error file validation : Menu fail ${isPassed}`);
  } catch (error) {
    throw new Error(`${error}`);
  }
}

/**
 * Reads the 2 arrays in
 * 
 * @param userFile 
 * @param menus 
 * @returns Array<{username: string; menuItems: string[];}>
 */
function userFileBase(userFile: any[], menus: any[]){
  try {
    const users: any[] = []
    const menuArr = menuCheck(menus)
    const regex = new RegExp(`^[yYnN]{${menuArr.length}}$`);
    userFile.forEach(( userLine ) => {

    const permissionArr = []
    const userName = userLine.substring(0, userLine.indexOf(' '))
    let rolesArr
    if (userName) {
      rolesArr = userLine.substring(userLine.indexOf(' ') + 1).split(" ").join('').toLowerCase()
      if(rolesArr.length === menuArr.length && regex.test(rolesArr)){
        for (let i = 0; i < rolesArr.length; i++) {
          if(rolesArr[i] === 'y'){
            permissionArr.push(menuArr[i])
          }
        }
      }else{
        throw new Error(`users.txt ${rolesArr}`);
      }
      users.push({userName: userName, menuItems: permissionArr})
    } 
  });
    return users;
  } catch (error: any) {
    console.error(`Error reading file : ${error.message}`);
    process.exit(1);
  }
  
}

/**
 * Reads file and returns a array of strings, the 
 * each line is a array entry
 * 
 * @param filePath 
 * @returns string[]
 */
function readFileContent(filePath: string): string[] {
  try {
    return fs.readFileSync(filePath, 'utf-8').split("\n");
  } catch (error: any) {
    throw new Error(`reading file ${filePath}: ${error.message}`);
  }
}
