"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = __importStar(require("fs"));
try {
    if (process.argv.length !== 4) {
        console.error('Usage: node app.js <file1> <file2>');
        process.exit(1);
    }
    let menuPath = '';
    let userPath = '';
    process.argv.forEach((item) => {
        var _a, _b;
        if ((_a = item === null || item === void 0 ? void 0 : item.toLocaleLowerCase()) === null || _a === void 0 ? void 0 : _a.includes('users.txt')) {
            userPath = item;
        }
        else if ((_b = item === null || item === void 0 ? void 0 : item.toLocaleLowerCase()) === null || _b === void 0 ? void 0 : _b.includes('menus.txt')) {
            menuPath = item;
        }
    });
    if (!menuPath && !userPath) {
        throw new Error(`Upload both files`);
    }
    const contentMenu = readFileContent(menuPath);
    const contentUser = readFileContent(userPath);
    if (!(contentUser === null || contentUser === void 0 ? void 0 : contentUser.length) && !(contentMenu === null || contentMenu === void 0 ? void 0 : contentMenu.length)) {
        throw new Error(`Files not converted`);
    }
    const users = { "users": userFileBase(contentUser, contentMenu) };
    // Check if the output is being redirected
    if (process.stdout.isTTY) {
        let outputFilePath = 'output.json';
        fs.writeFileSync(outputFilePath, JSON.stringify(users, null, 2));
    }
    else {
        console.log(JSON.stringify(users, null, 2));
    }
}
catch (error) {
    console.error(`Error : ${error.message}`);
    process.exit(1);
}
////////////////////////////////FUNCTIONS\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/**
 * Random number generator
 *
 * @param menusLength
 * @returns number[]
 */
function randomNumberGen(menusLength) {
    return Array(10).fill(0).map(() => Math.round(Math.random() * menusLength));
}
/**
 * validates and checks the menu
 *
 * @param menus
 * @returns string[]
 */
function menuCheck(menus) {
    try {
        const menuRegex = /^\d+,\s*.*?\bmenu\b/i;
        const isPassed = menus.reduce((accumulator, currentValue) => {
            return accumulator && menuRegex.test(currentValue);
        }, true);
        if (isPassed) {
            return menus.map((menu) => { var _a; return (_a = menu.substring(menu.indexOf(',') + 1)) === null || _a === void 0 ? void 0 : _a.trim(); });
        }
        throw new Error(`Error file validation : Menu fail ${isPassed}`);
    }
    catch (error) {
        throw new Error(`${error}`);
    }
}
/**
 * Reads the 2 arrays in
 *
 * @param userFile
 * @param menus
 * @returns Array<{username: string; menuItems: string[];}>
 */
function userFileBase(userFile, menus) {
    try {
        const users = [];
        const menuArr = menuCheck(menus);
        const regex = new RegExp(`^[yYnN]{${menuArr.length}}$`);
        userFile.forEach((userLine) => {
            const permissionArr = [];
            const userName = userLine.substring(0, userLine.indexOf(' '));
            let rolesArr;
            if (userName) {
                rolesArr = userLine.substring(userLine.indexOf(' ') + 1).split(" ").join('').toLowerCase();
                if (rolesArr.length === menuArr.length && regex.test(rolesArr)) {
                    for (let i = 0; i < rolesArr.length; i++) {
                        if (rolesArr[i] === 'y') {
                            permissionArr.push(menuArr[i]);
                        }
                    }
                }
                else {
                    throw new Error(`users.txt ${rolesArr}`);
                }
                users.push({ userName: userName, menuItems: permissionArr });
            }
        });
        return users;
    }
    catch (error) {
        console.error(`Error reading file : ${error.message}`);
        process.exit(1);
    }
}
/**
 * Reads file and returns a array of strings, the
 * each line is a array entry
 *
 * @param filePath
 * @returns string[]
 */
function readFileContent(filePath) {
    try {
        return fs.readFileSync(filePath, 'utf-8').split("\n");
    }
    catch (error) {
        throw new Error(`reading file ${filePath}: ${error.message}`);
    }
}
